"use strict"

/*

у js асинхронними є setTimeout and setInterval й усе що стосується типу промісів, все окрім цього
відразу виконується, а типи асинхронних компонентів спочатку попадають у Web IPA а опісля уже виконуются.

 */

//--------------------------------------------------------------------------------------------------

const buttonFindMe = document.querySelector("button");

const findFunction = async () => {
    try{
        const {data} = await axios('https://api.ipify.org/?format=json');
        const {ip} = data;

        await axios(`http://ip-api.com/json/${ip}`)
            .then(({data}) => {
                document.body.insertAdjacentHTML("beforeend",`
                <ul>
                     <li>timezone:  ${data.timezone}</li>
                     <li>country:  ${data.country}</li>
                     <li>region:  ${data.regionName}</li>
                     <li>city:  ${data.city}</li>
                </ul>`)
            })
    }catch (err) {
        console.log(err)
    }
};


buttonFindMe.addEventListener("click", (ev) =>{
    ev.preventDefault();
    findFunction();
});
